<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware'=>['auth']], function (){
/* Gambar*/
	Route::get('/admin', 'GambarController@viewGambar');
	//index
    Route::get('/admin/gambar', 'GambarController@viewGambar');
	//create
	Route::get('/admin/gambar/create', 'GambarController@create');
    Route::post('/admin/gambar/store', 'GambarController@createGambar');
	//edit
	Route::get('/admin/gambar/edit/{id}', 'GambarController@edit');
    Route::post('/admin/gambar/update/', 'GambarController@updateGambar');
    //delete
    Route::get('/admin/gambar/delete/{id}', 'GambarController@deleteGambar');
    //search
    Route::get('/admin/gambar/search', 'GambarController@searchGambar');
	//status
	Route::get('/admin/gambar/change/{id}', 'GambarController@changeStatusGambar');
/* Video*/
	//index
    Route::get('/admin/video', 'VideoController@viewVideo');
	//create
	Route::get('/admin/video/create', 'VideoController@create');
    Route::post('/admin/video/store', 'VideoController@createVideo');
	//edit
	Route::get('/admin/video/edit/{id}', 'VideoController@edit');
    Route::post('/admin/video/update/', 'VideoController@updateVideo');
    //delete
    Route::get('/admin/video/delete/{id}', 'VideoController@deleteVideo');
    //search
    Route::get('/admin/video/search', 'VideoController@searchVideo');
	//status
	Route::get('/admin/video/change/{id}', 'VideoController@changeStatusVideo');

/* Berita*/
	//index
    Route::get('/admin/berita', 'BeritaController@viewBerita');
	//create
	Route::get('/admin/berita/create', 'BeritaController@create');
    Route::post('/admin/berita/store', 'BeritaController@createBerita');
	//edit
	Route::get('/admin/berita/edit/{id}', 'BeritaController@edit');
    Route::post('/admin/berita/update/', 'BeritaController@updateBerita');
    //delete
    Route::get('/admin/berita/delete/{id}', 'BeritaController@deleteBerita');
    //search
    Route::get('/admin/berita/search', 'BeritaController@searchBerita');
	//status
	Route::get('/admin/berita/change/{id}', 'BeritaController@changeStatusBerita');

/* Pengaturan*/
	//index
    Route::get('/admin/pengaturan', 'PengaturanController@viewPengaturan');
});