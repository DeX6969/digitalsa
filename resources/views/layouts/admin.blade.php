<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Sistem Informasi Administrasi Departemen Ilmu Komputer/Informatika</title>
	<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
	<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <link rel="stylesheet" href="{{ URL::asset('css/dashboard.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}" />
</head>
<body>
    <header class="container-fluid zhm-navbar">

      <nav class="navbar navbar-custom2 navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">ADMIN</a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">

			   <li>
                     <a href="{{ url('/logout') }}"
                         onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
                         <i class="fa fa-fw fa-power-off"></i> Log Out
                     </a>
					<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">  {{ csrf_field() }}</form>
               </li>
            </ul>
          </div>
        </div>
      </nav>
    </header>

    <div class="col-sm-3 col-md-2 sidebar">
        <div class="row placeholders text-center">
			<div class="col-xs-6 col-sm-12 placeholder"><br><br>
              <img src="{{ URL::asset('images/Elang.png') }}" width="100" height="100" class="img-responsive" alt="Generic placeholder thumbnail"><br><br>
			</div>

        </div>
        <ul class="nav nav-sidebar">
            <li><a href="/admin/gambar"><b>Gambar</b></a></li>
            <li><a href="/admin/video"><b>Video</b></a></li>
            <li><a href="/admin/berita"><b>Berita</b></a></li>
			<li><a href="/admin/pengaturan"><b>Pengaturan</b></a></li>
        </ul>
    </div>
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		@yield('content')
	</div>
	<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>

</body>
</html>
