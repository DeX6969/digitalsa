<!DOCTYPE html>
<html>
<style>
body  {
	background-image:url({{ URL::asset('images/intro-bg.jpg') }});
    background-size: 100%;
}
</style>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Digital Sign Age Jurusan Fisika FSM UnDip</title>
	<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}">
	<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <link rel="stylesheet" href="{{ URL::asset('css/dashboard.css') }}" />
    <link rel="stylesheet" href="{{ URL::asset('css/custom.css') }}" />
</head>
<body>
	<div class="bgImage">
	 <nav class="navbar navbar-custom navbar-fixed-top">
        <div class="container-fluid">
          <div class="col-sm-12">
            <marquee style="color:white; font-size:350%; padding-top:5px; margin-right:10px">SELAMAT DATANG DI JURUSAN FISIKA FAKULTAS SAINT DAN MATEMATIKA</marquee>
          </div>
        </div>
		<a class="marcus" href="/admin"><span class="glyphicon glyphicon-pencil"></span></a>
    </nav>

	<div class="main">
		@yield('content')
	</div>
	<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
	
	</div>
</body>
</html>

<script type="text/javascript" language="javascript">
function renderTime(){
 var currentTime = new Date();
 var h = currentTime.getHours();
 var m = currentTime.getMinutes();
 var s = currentTime.getSeconds();
 if (h == 0){
  h = 24;
   }
   if (h < 10){
    h = "0" + h;
    }
    if (m < 10){
    m = "0" + m;
    }
    if (s < 10){
    s = "0" + s;
    }   
 var myClock = document.getElementById('clockDisplay');
 myClock.textContent = h + ":" + m + ":" + s + "";
 var myClock2 = document.getElementById('dateDisplay');
 myClock2.textContent = currentTime.getDate()+"/"+(currentTime.getMonth()+1)+"/"+currentTime.getFullYear()+"";     
 setTimeout ('renderTime()',1000);
}
 renderTime();
</script>