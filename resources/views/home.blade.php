@extends('layouts.app')
@section('content')
<div class="row">	
	<div class="col-sm-6">
		<div class="slideshow-container">
		<?php $jml=0;?>
		@foreach($gambars->sortBy('id') as $gambars)
			<?php if($gambars->status=="Tampilkan"){?>
			<div class="mySlides fade">
			  <img src="uploads/{{$gambars->file}}" style="width:100%; height:600px; border-radius:5%;">
			</div>
			<?php $jml+=1;}?>
		@endforeach
			<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
			<a class="next" onclick="plusSlides(1)">&#10095;</a>
		</div>
		<br>

		<div style="text-align:center">
		<?php for($i=0;$i<$jml;$i++){?>
		  <span class="dot" ></span>
		<?php }?>
		</div>
	</div>
	<div class="col-sm-6">
		@foreach($videos->sortBy('id') as $videos)
			<?php if($videos->status=="Tampilkan"){?>
				<video class="myVid" width="100%" height="600px" muted loop autoplay src="{{ URL::asset("uploads/{$videos->file}")}}">
			<?php }?>
		@endforeach
	</div>
</div>	
	
<div class="footer navbar-fixed-bottom">
	<div class="col-sm-11">
	<marquee style="font-size:200%;"><p>
	@foreach($beritas->sortBy('id') as $beritas)
		<?php if($beritas->status=="Tampilkan"){?>
			{{$beritas->berita}} &nbsp&nbsp # &nbsp&nbsp 
		<?php }?>
	@endforeach
	</p></marquee>
	</div>
	<div class="col-sm-1">
		<b><div class="row center" id="clockDisplay"></div></b>
		<b><div class="row center" id="dateDisplay"></div></b>
	</div>
</div>

<script>
//var k = 0;
var slideIndex = 0;
showSlides();
//showSlides2();

function showSlides() {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");	
    for (i = 0; i < slides.length; i++) {
       slides[i].style.display = "none";  
    }
    slideIndex++;
    if (slideIndex> slides.length) {slideIndex = 1}    
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";  
    dots[slideIndex-1].className += " active";
    setTimeout(showSlides, 2000); // Change image every 2 seconds
}
/*function showSlides2() {  
	var j;
    var video = document.getElementsByClassName("myVid");
    for (j = 0; j < video.length; j++) {
		video[j].style.display = "none";
		video[j].muted = true;
    }
    if (k == 0) {video[0].style.display = "block";  k=1; video[0].muted = false;}
	else {if (k == 1) {  k=0; }}
    setTimeout(showSlides2, 20000); // Non active display video every 20 seconds
}*/
</script>
@endsection