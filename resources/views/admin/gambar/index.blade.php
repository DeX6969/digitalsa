@extends('layouts.admin')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-2">
					<h3>Gambar</h3>
				</div>
				<div class="col-md-10">
					<a href="{{ url('/admin/gambar/create') }}" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-plus"></span> Tambah Data</a><br><br>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-body">
					@if(Session::has('alert-success'))
					    <div class="alert alert-success">
				            {{ Session::get('alert-success') }}
				        </div>
					@endif

					<div class="row">
						<div class="col-xs-6">
                            	
                        </div>
                        <div class="col-xs-6">
							<form method="GET" action="{{ url('/admin/gambar/search') }}">
								<div class="form-group col-xs-10">
									<input type="text" name="s" class="form-control" placeholder="Cari Berdasarkan Judul">
								</div>
								<div class="form-group col-xs-2" style="right:10px;">
									<button class="btn btn-success"><span class="glyphicon glyphicon-search"></span> Cari</button>
								</div>
							</form>
						</div>
                    </div>

					<table class="table table-bordered">
						<tr>
							<th style="text-align:center; width:5%;">No</th>
							<th style="text-align:center; width:20%;">Judul</th>
							<th style="text-align:center; width:45%;">Keterangan</th>
							<th style="text-align:center; width:15%;">Gambar</th>
							<th style="text-align:center; width:10%;">Status</th>
							<th style="text-align:center; width:5%;">Pilihan</th>
						</tr>
						<?php $no=1; ?>
						@foreach($gambars->sortBy('id') as $gambars)
						<tr>
							<td style="text-align:right;">{{$no++}}</td>
							<td>{{$gambars->judul}}</td>
							<td>{{$gambars->keterangan}}</td>
							<td style="text-align:center;"><a target="_blank" href="{{ URL::asset("uploads/{$gambars->file}")}}"><img style="width:170px;height:150px;" src="{{ URL::asset("uploads/{$gambars->file}")}}"></td>
							<td style="text-align:center;">{{$gambars->status}}</td>
							<td class="text-center">
								<div class="dropdown">
									<a href="#" class="dropdown-toggle btn btn-primary" type="button" data-toggle="dropdown"><span class="glyphicon glyphicon-chevron-down"></span></a>
									<ul class="dropdown-menu">
										<li><?php echo '<a onclick="return konfirmasiSetuju()" data-toggle="modal" data-id="#" class="openEditObat" href="';?> {{url('/admin/gambar/change/'.$gambars->id)}}" ><span class="glyphicon glyphicon-refresh"></span> Ubah</a></li>
										<li><?php echo '<a onclick="return konfirmasi()" href="';?>{{url('/admin/gambar/delete/'.$gambars->id)}}<?php echo '"><span class="glyphicon glyphicon-trash"></span> Hapus</a></li>';?>
									</ul>
								</div>
							</td>
						</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function konfirmasiSetuju(){
		tanya = confirm("Anda Yakin Akan Mengubah Status Data?");
		if (tanya == true) return true;
		else return false;
    }
	function konfirmasi(){
		tanya = confirm("Anda Yakin Akan Menghapus Data ?");
		if (tanya == true) return true;
		else return false;
    }
</script>
@endsection
