@extends('layouts.admin')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<h3>Tambah Berita</h3>
			<div class="panel panel-default">
				<div class="panel-body">
					<form enctype="multipart/form-data" action="{{ url('/admin/berita/store')}}" method="post">
					{{csrf_field()}}
						<div class="form-group{{ $errors->has('judul') ? ' has-error' : '' }}">
							<label for="judul">Judul</label>
							<input type="text" name="judul" class="form-control" placeholder="Judul" value="{{ old('judul') }}">
							{!! $errors->first('judul', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('berita') ? ' has-error' : '' }}">
							<label for="berita">Berita</label>
							<textarea name="berita" class="form-control" placeholder="Berita" >{{ old('berita') }}</textarea>
							{!! $errors->first('berita', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
							<label for="status">Status</label><br>
							<input type="radio" name="status"  value="Tampilkan" checked> Tampilkan<br>
							<input type="radio" name="status"  value="Tidak"> Tidak<br>
							{!! $errors->first('status', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group">
							<input type="submit" class="btn btn-primary" value="Simpan">
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
