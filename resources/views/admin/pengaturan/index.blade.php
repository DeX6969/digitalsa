@extends('layouts.admin')
@section('content')
<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-2">
					<h3>Pengaturan</h3>
				</div>
				<div class="col-md-10">
					<a href="{{ url('/') }}" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-plus"></span> Tambah Data</a><br><br>
				</div>
			</div>
			<div class="panel panel-default">
				<div class="panel-body">
					@if(Session::has('alert-success'))
					    <div class="alert alert-success">
				            {{ Session::get('alert-success') }}
				        </div>
					@endif

					<div class="row">
						<div class="col-md-1">
                            	<button type="button" class="btn btn-default" data-toggle="modal" data-target="#filterModal"><span class="glyphicon glyphicon-filter"></span> Saring</button>
                        </div>
						<div class="col-md-4">
							<div class="col-md-12">
									<button type="button" class="btn btn-default" disabled><span class="glyphicon glyphicon-download"></span> Rekapitulasi</button>
							</div>
                        </div>
                        <div class="col-md-7">
							<form method="GET" action="{{ url('/admin/suratmasuk/search') }}">
								<div class="form-group col-md-10">
									<input type="text" name="s" class="form-control" placeholder="Cari...">
								</div>
								<div class="form-group">
									<button class="btn btn-success"><span class="glyphicon glyphicon-search"></span> Cari</button>
								</div>
							</form>
						</div>
                    </div>

					<table class="table table-bordered">
						<tr>
							<th style="text-align:center;">No</th>
							<th style="text-align:center;">Durasi Gambar</th>
							<th style="text-align:center;">Durasi Halaman</th>
							<th style="text-align:center;">Kecepatan Berita</th>
							<th style="text-align:center;">Lamp. SO</th>
							<th style="text-align:center;">Lamp. VM</th>
							<th style="text-align:center;">Pilihan</th>
						</tr>
						<?php $no=1; ?>
						@foreach($pengaturans->sortBy('id') as $pengaturans)
						<tr>
							<td>{{$no++}}</td>
							<td>{{$pengaturans->durasiGambar}}</td>
							<td>{{$pengaturans->durasiHalaman}}</td>
							<td>{{$pengaturans->kecepatanBerita}}</td>
							<td>{{$pengaturans->strukturOrganisasi}}</td>
							<td>{{$pengaturans->visiMisi}}</td>
							<td class="text-center">
								<div class="dropdown">
									<a href="#" class="dropdown-toggle btn btn-primary" type="button" data-toggle="dropdown"><span class="glyphicon glyphicon-chevron-down"></span></a>
									<ul class="dropdown-menu">
										<li><a data-toggle="modal" data-id="#" class="openEditObat" href="/admin/gambar" ><span class="glyphicon glyphicon-pencil"></span> Ubah</a></li>
										<li><?php echo '<a onclick="return konfirmasi()" href="/admin/gambar"><span class="glyphicon glyphicon-trash"></span> Hapus</a></li>';?>
									</ul>
								</div>
							</td>
						</tr>
						@endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal -->
<div class="modal fade" id="filterModal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Saring Berdasarkan Bulan & Tahun</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
					<form method="GET" action="{{ url('/admin/suratmasuk/filter') }}">
						<div class="form-group">
							<label for="bulan">Bulan</label>
							<select name="bulan" class="form-control">
								@if($errors->has('tahun')) <option id='bulani' value='{{ old("bulan") }}'></option>@endif
								
								<option value='00'>-</option>
								<option value='01'>Januari</option>
								<option value='02'>Februari</option>
								<option value='03'>Maret</option>
								<option value='04'>April</option>
								<option value='05'>Mei</option>
								<option value='06'>Juni</option>
								<option value='07'>Juli</option>
								<option value='08'>Agustus</option>
								<option value='09'>September</option>
								<option value='10'>Oktober</option>
								<option value='11'>November</option>
								<option value='12'>Desember</option>
							</select>
						</div>
						<div class="form-group{{ $errors->has('tahun') ? ' has-error' : '' }}">
							<label for="tahun">Tahun</label>
							<input type="number" class="form-control" name="tahun" @if($errors->has('tahun')) autofocus @endif placeholder="Tahun" min="2000">
							{!! $errors->first('tahun', '<p class="help-block">:message</p>') !!}
						</div>
						<div class="form-group">
							<button class="btn btn-primary">Saring</button>
						</div>
					</form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
@if($errors->has('tahun')) $(document).ready(function(){ $("#filterModal").modal("show");}); @endif
function bulan(a){
	if (a=='00') return '-';
	else if  (a=='01') return 'Januari';
	else if  (a=='02') return 'Februari';
	else if  (a=='03') return 'Maret';
	else if  (a=='04') return 'April';
	else if  (a=='05') return 'Mei';
	else if  (a=='06') return 'Juni';
	else if  (a=='07') return 'Juli';
	else if  (a=='08') return 'Agustus';
	else if  (a=='09') return 'September';
	else if  (a=='10') return 'Oktober';
	else if  (a=='11') return 'November';
	else if  (a=='12') return 'Desember';
}
document.getElementById("bulani").innerHTML = bulan({{ old("bulan")}});
</script>
@endsection
