<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pengaturan;
use DB;
use Redirect;
use \Input as Input;

class PengaturanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewPengaturan()
    {
		$pengaturans = Pengaturan::all();
        return view('admin.pengaturan.index', compact('pengaturans'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.suratmasuk.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createSuratMasuk(StoreRequest $request)
    {
		$surat_masuks = new SuratMasuk();
		$surat_masuks->nomor=$request->nomor;
		$surat_masuks->pengirim=$request->pengirim;
		$surat_masuks->hp=$request->hp;
		$surat_masuks->tanggal=$request->tanggal;
		$surat_masuks->perihal=$request->perihal;

		if(Input::hasFile('file')){
			$file= Input::file('file');
			$file->move('uploads',$file->getClientOriginalName());
            $surat_masuks->gambar = $file->getClientOriginalName();
		}
		$surat_masuks->save();
		return  redirect('/admin/suratmasuk')->with('alert-success', 'Data Berhasil Ditambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $surat_masuks = SuratMasuk::findOrFail($id)	;
        return view('admin.suratmasuk.edit', compact('surat_masuks'));
		//return view('edit')->with('surat_masuks',$surat_masuks);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateSuratMasuk(UpdateRequest $request)
    {
		$id = $request->id;
        $surat_masuks = SuratMasuk::findOrFail($id);
		$surat_masuks->nomor=$request->nomor;
		$surat_masuks->pengirim=$request->pengirim;
		$surat_masuks->hp=$request->hp;
		$surat_masuks->tanggal=$request->tanggal;
		$surat_masuks->perihal=$request->perihal;
        if(Input::hasFile('file')){
			$file= Input::file('file');
			$file->move('uploads',$file->getClientOriginalName());
            $surat_masuks->gambar = $file->getClientOriginalName();
		}
        $surat_masuks->save();
	    return  redirect('/admin/suratmasuk')->with('alert-success', 'Data Berhasil Diubah.');
   }

   /**
    * Display search result.
    *
    * @return \Illuminate\Http\Response
    */
   public function filterSuratMasuk(FilterRequest $request)
   {
       if ($request->bulan != '00'){
           $surat_masuks = SuratMasuk::whereMonth('tanggal', $request->bulan) -> whereYear('tanggal', $request->tahun)->paginate(10);
           $surat_masuks->bulan = $request->bulan;
           $surat_masuks->tahun = $request->tahun;

       }
       else {
           $surat_masuks = SuratMasuk::whereYear('tanggal', $request->tahun)->paginate(10);
           $surat_masuks->bulan = $request->bulan;
           $surat_masuks->tahun = $request->tahun;

       }
       return view('admin.suratmasuk.filter', compact('surat_masuks'));
   }

   /**
    * Display search result.
    *
    * @return \Illuminate\Http\Response
    */
	public function rekapitulasiSuratMasuk(Request $request)
    {
        if ($request->bulan != '00'){
            $surat_masuks = SuratMasuk::whereMonth('tanggal', $request->bulan) -> whereYear('tanggal', $request->tahun)->paginate(10);
            $surat_masuks->bulan = $request->bulan;
            $surat_masuks->tahun = $request->tahun;

        }
        else {
            $surat_masuks = SuratMasuk::whereYear('tanggal', $request->tahun)->paginate(10);
            $surat_masuks->bulan = $request->bulan;
            $surat_masuks->tahun = $request->tahun;
        }
        return view('admin.suratmasuk.rekapitulasi', compact('surat_masuks'));
    }
}

