<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\Gambar\StoreRequest;
use App\Http\Requests\Gambar\UpdateRequest;
use App\Http\Requests\Gambar\FilterRequest;
use App\Gambar;
use DB;
use Redirect;
use \Input as Input;

class GambarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewGambar()
    {
		$gambars = Gambar::all();
        return view('admin.gambar.index', compact('gambars'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.gambar.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createGambar(StoreRequest $request)
    {
		$gambars = new Gambar();
		$gambars->judul=$request->judul;
		$gambars->status=$request->status;
		$gambars->keterangan=$request->keterangan;
		
		if($request->hasFile('file')){
			$file= $request->file('file');
			$file->move('uploads',$gambars->judul. '.' . $file->getClientOriginalExtension());
            $gambars->file = $gambars->judul. '.' . $file->getClientOriginalExtension();
		}
		$gambars->save();
		return  redirect('/admin/gambar')->with('alert-success', 'Data Berhasil Ditambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $gambars = SuratMasuk::findOrFail($id)	;
        return view('admin.suratmasuk.edit', compact('gambars'));
		//return view('edit')->with('gambars',$gambars);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateGambar(UpdateRequest $request)
    {
		$id = $request->id;
        $gambars = SuratMasuk::findOrFail($id);
		$gambars->nomor=$request->nomor;
		$gambars->pengirim=$request->pengirim;
		$gambars->hp=$request->hp;
		$gambars->tanggal=$request->tanggal;
		$gambars->perihal=$request->perihal;
        if(Input::hasFile('file')){
			$file= Input::file('file');
			$file->move('uploads',$file->getClientOriginalName());
            $gambars->gambar = $file->getClientOriginalName();
		}
        $gambars->save();
	    return  redirect('/admin/suratmasuk')->with('alert-success', 'Data Berhasil Diubah.');
   }
   
   /**
     * Display search result.
     *
     * @return \Illuminate\Http\Response
     */
   public function deleteGambar($id)
   {
		$gambars = Gambar::findOrFail($id);
		$gambars->delete();
        return  redirect('/admin/gambar')->with('alert-success', $gambars->judul .' Berhasil Dihapus');
   }
   
   /**
     * Display search result.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchGambar(Request $request)
    {
        //$search = Request::get('s');
        $gambars = Gambar::where('judul','like','%'.$request->s.'%')->orderBy('id')->paginate(10);
        return view('admin.gambar.index', compact('gambars'));
    }
	
	/**
     * Display search result.
     *
     * @return \Illuminate\Http\Response
     */
	public function changeStatusGambar($id)
	{
		$gambars = Gambar::findOrFail($id);
		
        if($gambars->status == 'Tampilkan') {
            $gambars->status = 'Tidak';
            $gambars->save();
            return  redirect('/admin/gambar')->with('alert-success', 'Status '. $gambars->judul .' Berhasil Diubah Menjadi Tidak.');
        }
		elseif($gambars->status == 'Tidak') {
            $gambars->status = 'Tampilkan';
            $gambars->save();
            return  redirect('/admin/gambar')->with('alert-success', 'Status '. $gambars->judul .' Berhasil Diubah Menjadi Tampilkan.');
        }
   }
}