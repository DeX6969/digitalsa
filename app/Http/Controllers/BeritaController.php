<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\Berita\StoreRequest;
use App\Http\Requests\Berita\UpdateRequest;
use App\Http\Requests\Berita\FilterRequest;
use App\Berita;
use DB;
use Redirect;
use \Input as Input;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewBerita()
    {
		$beritas = Berita::all();
        return view('admin.berita.index', compact('beritas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.berita.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createBerita(StoreRequest $request)
    {
		$beritas = new Berita();
		$beritas->judul=$request->judul;
		$beritas->status=$request->status;
		$beritas->berita=$request->berita;
		$beritas->save();
		return  redirect('/admin/berita')->with('alert-success', 'Data Berhasil Ditambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Beritas = SuratMasuk::findOrFail($id)	;
        return view('admin.suratmasuk.edit', compact('Beritas'));
		//return view('edit')->with('Beritas',$Beritas);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateBerita(UpdateRequest $request)
    {
		$id = $request->id;
        $Beritas = SuratMasuk::findOrFail($id);
		$Beritas->nomor=$request->nomor;
		$Beritas->pengirim=$request->pengirim;
		$Beritas->hp=$request->hp;
		$Beritas->tanggal=$request->tanggal;
		$Beritas->perihal=$request->perihal;
        if(Input::hasFile('file')){
			$file= Input::file('file');
			$file->move('uploads',$file->getClientOriginalName());
            $Beritas->file = $file->getClientOriginalName();
		}
        $Beritas->save();
	    return  redirect('/admin/suratmasuk')->with('alert-success', 'Data Berhasil Diubah.');
   }
   
   /**
     * Display search result.
     *
     * @return \Illuminate\Http\Response
     */
   public function deleteBerita($id)
   {
		$beritas = Berita::findOrFail($id);
		$beritas->delete();
        return  redirect('/admin/berita')->with('alert-success', $beritas->judul .' Berhasil Dihapus');
   }
   
   /**
     * Display search result.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchBerita(Request $request)
    {
        //$search = Request::get('s');
        $beritas = Berita::where('judul','like','%'.$request->s.'%')->orderBy('id')->paginate(10);
        return view('admin.berita.index', compact('beritas'));
    }
	
	/**
     * Display search result.
     *
     * @return \Illuminate\Http\Response
     */
	public function changeStatusBerita($id)
	{
		$beritas = Berita::findOrFail($id);
		
        if($beritas->status == 'Tampilkan') {
            $beritas->status = 'Tidak';
            $beritas->save();
            return  redirect('/admin/berita')->with('alert-success', 'Status '. $beritas->judul .' Berhasil Diubah Menjadi Tidak.');
        }
		elseif($beritas->status == 'Tidak') {
            $beritas->status = 'Tampilkan';
            $beritas->save();
            return  redirect('/admin/berita')->with('alert-success', 'Status '. $beritas->judul .' Berhasil Diubah Menjadi Tampilkan.');
        }
   }
}