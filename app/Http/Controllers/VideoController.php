<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\Video\StoreRequest;
use App\Http\Requests\Video\UpdateRequest;
use App\Http\Requests\Video\FilterRequest;
use App\Video;
use DB;
use Redirect;
use \Input as Input;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function viewVideo()
    {
		$videos = Video::all();
        return view('admin.video.index', compact('videos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.video.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function createVideo(StoreRequest $request)
    {
		$videos = new Video();
		$videos->judul=$request->judul;
		$videos->status=$request->status;
		$videos->keterangan=$request->keterangan;
		
		if($request->hasFile('file')){
			$file= $request->file('file');
			$file->move('uploads',$videos->judul. '.' . $file->getClientOriginalExtension());
            $videos->file = $videos->judul. '.' . $file->getClientOriginalExtension();
		}
		$videos->save();
		return  redirect('/admin/video')->with('alert-success', 'Data Berhasil Ditambahkan.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $videos = SuratMasuk::findOrFail($id)	;
        return view('admin.suratmasuk.edit', compact('videos'));
		//return view('edit')->with('videos',$videos);
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateVideo(UpdateRequest $request)
    {
		$id = $request->id;
        $videos = SuratMasuk::findOrFail($id);
		$videos->nomor=$request->nomor;
		$videos->pengirim=$request->pengirim;
		$videos->hp=$request->hp;
		$videos->tanggal=$request->tanggal;
		$videos->perihal=$request->perihal;
        if(Input::hasFile('file')){
			$file= Input::file('file');
			$file->move('uploads',$file->getClientOriginalName());
            $videos->file = $file->getClientOriginalName();
		}
        $videos->save();
	    return  redirect('/admin/suratmasuk')->with('alert-success', 'Data Berhasil Diubah.');
   }
   
   /**
     * Display search result.
     *
     * @return \Illuminate\Http\Response
     */
   public function deleteVideo($id)
   {
		$videos = Video::findOrFail($id);
		$videos->delete();
        return  redirect('/admin/video')->with('alert-success', $videos->judul .' Berhasil Dihapus');
   }
   
   /**
     * Display search result.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchVideo(Request $request)
    {
        //$search = Request::get('s');
        $videos = Video::where('judul','like','%'.$request->s.'%')->orderBy('id')->paginate(10);
        return view('admin.video.index', compact('videos'));
    }
	
	/**
     * Display search result.
     *
     * @return \Illuminate\Http\Response
     */
	public function changeStatusVideo($id)
	{
		$videos = Video::findOrFail($id);
		
        if($videos->status == 'Tampilkan') {
            $videos->status = 'Tidak';
            $videos->save();
            return  redirect('/admin/video')->with('alert-success', 'Status '. $videos->judul .' Berhasil Diubah Menjadi Tidak.');
        }
		elseif($videos->status == 'Tidak') {
            $videos->status = 'Tampilkan';
            $videos->save();
            return  redirect('/admin/video')->with('alert-success', 'Status '. $videos->judul .' Berhasil Diubah Menjadi Tampilkan.');
        }
   }
}