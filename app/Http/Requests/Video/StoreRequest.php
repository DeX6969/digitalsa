<?php

namespace App\Http\Requests\Video;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            'judul'=> 'Required|unique:videos,judul',
        ];
    }

    public function messages()
    {
        return [
			'judul.required' => 'Judul tidak Boleh Kosong.',
			'judul.unique' => 'Judul tidak Boleh Sama',
        ];
    }
}
