<?php

namespace App\Http\Requests\suratmasuk;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            'nomor'=> 'Required|unique:surat_masuks,nomor',
			'pengirim'=> 'Required:surat_masuks,pengirim',
			'hp'=> 'Required:surat_masuks,hp',
			'tanggal'=> 'Required:surat_masuks,tanggal',
			'perihal'=> 'Required:surat_masuks,perihal',
        ];
    }

    public function messages()
    {
        return [
            'nomor.required' => 'Nomor Surat Tidak Boleh Kosong.',
            'nomor.unique' => 'Nomor Surat Sudah Ada.',
			'pengirim.required' => 'Pengirim Tidak Boleh Kosong.',
			'hp.required' => 'Nomor Handphone Tidak Boleh Kosong.',
			'tanggal.required' => 'Tanggal Tidak Boleh Kosong.',
			'perihal.required' => 'Perihal Surat Tidak Boleh Kosong.',
        ];
    }
}
