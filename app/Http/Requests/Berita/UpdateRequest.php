<?php

namespace App\Http\Requests\Berita;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules()
    {
        return [
            'judul'=> 'Required|unique',
			'judul'=> 'Required',
        ];
    }

    public function messages()
    {
        return [
			'judul.required' => 'Judul tidak Boleh Kosong.',
			'judul.unique' => 'Judul tidak Boleh Sama',
			'berita.required' => 'Berita tidak Boleh Kosong.',
        ];
    }
}
