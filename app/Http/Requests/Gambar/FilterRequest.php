<?php

namespace App\Http\Requests\Gambar;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;


class FilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
     public function rules(Request $request)
    {
        return [
			'tahun'=> 'Required',
        ];
    }

    public function messages()
    {
        return [
			'tahun.required' => 'Tahun Tidak Boleh Kosong.',
        ];
    }
}
